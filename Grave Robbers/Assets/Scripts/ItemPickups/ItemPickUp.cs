﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemPickUp : MonoBehaviour
{
    //base class for powerups/ pickups

    public enum ItemName { Key, Sova, Miaosplosion, Boomerang };
    public ItemName itemName;

    public bool expiresImmediately;             //for things such as keys that expire on contact vs powerups that last
    public GameObject vfx;
    public AudioSource sfx;

    //GameObject player;

    Collider col;
    MeshRenderer rend;
    protected PlayerShooting altFire;

    protected enum PowerUpState { isEmpty, isActive, isExpiring }
    protected PowerUpState powerUpState;

    protected virtual void Awake()
    {
        col = GetComponent<Collider>();
        rend = GetComponent<MeshRenderer>();
    }

    protected virtual void Start()
    {
        powerUpState = PowerUpState.isEmpty;
    }

    public virtual void OnTriggerEnter(Collider other)
    {
        Pickup(other.gameObject);      
    }

    protected virtual void Pickup(GameObject player)
    {
        if (player.tag != "Player")              //ignore if is NOT player
        {
            return;
        }
        powerUpState = PowerUpState.isActive;

        PlayerCounter counter = player.GetComponent<PlayerCounter>();

        if (itemName != ItemName.Key)
        {
            counter.powerUpsCollected += 1;
            rend.enabled = false;       // Disables renderer (will destroy gameobject later)
        }
        else
        {
            EndLevel.instance.keysObtained += 1;
            counter.goldKeys += 1;
            transform.Find("Circle").GetComponent<MeshRenderer>().enabled = false;
        }

        // Reference to PlayerShooting
        altFire = player.GetComponent<PlayerShooting>();

        Debug.Log(itemName + " item obtained");

        // Power Up Effects
        PowerUpEffect();

        // Power Up Ability
        PowerUpPayLoad();

        col.enabled = false;        // Disables collider (will destroy gameobject later)     
    }

    protected virtual void PowerUpEffect()
    {
        if (vfx != null)
        {
            Instantiate(vfx, transform.position, transform.rotation, transform);
        }

        if (sfx != null)
        {
            sfx.Play();
        }
    }

    protected virtual void PowerUpPayLoad()
    {
        if (expiresImmediately)
        {
            PowerUpHasExpired();
        }
    }

    protected virtual void PowerUpHasExpired()
    {
        if (powerUpState == PowerUpState.isExpiring)
        {
            return;
        }
        powerUpState = PowerUpState.isExpiring;

        Debug.Log("Powerup expiring");
    }

    protected virtual void DestroyAfterDelay(float delay)
    {
        Destroy(gameObject, delay);
    }
}
