﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Item_GoldKey : ItemPickUp
{
    protected override void PowerUpPayLoad()
    {
        base.PowerUpPayLoad();
        DestroyAfterDelay(2f);
    }
}
