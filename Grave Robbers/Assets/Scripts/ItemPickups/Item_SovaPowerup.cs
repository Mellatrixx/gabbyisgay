﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Item_SovaPowerup : ItemPickUp
{
    public float timer = 4f;
    bool isReffed = false; // is referenced
    protected override void PowerUpPayLoad()
    {
        base.PowerUpPayLoad();
        altFire.sova = true;
        altFire.miaosplosion = false;
        altFire.boomerang = false;

        isReffed = true;
        DestroyAfterDelay(6f);
    }

    private void Update()
    {
        if (isReffed) 
        { 
            timer -= Time.deltaTime;
            if(timer <= 0f && altFire != null)
            {
                altFire.sova = false;
            }
        }
    }
}
