﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(CharacterController))]

public class PlayerController : MonoBehaviour
{

    Animator animator;
    CharacterController controller;
    public Camera mainCam;
    CameraController camControl;

    float x;
    float z;
    Vector3 zeroDir = Vector3.zero;

    public float speed = 6f;
    public float health;
    public float maxHealth = 10f;
    public float gravity = 10f;

    public Image healthBar;

    public LayerMask layerMask;

    private void Start()
    {
        health = maxHealth;
        controller = GetComponent<CharacterController>();
        animator = GetComponentInChildren<Animator>();
        camControl = mainCam.GetComponent<CameraController>();
    }

    private void Update()
    {
        PlayerMove();
        PlayerLook();
        UpdateHealth();
        Die();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Maze")
        {
            Debug.Log("Entered Maze");
            mainCam.transform.Rotate(new Vector3(35, 0, 0));
            camControl.offset = new Vector3(0, 20, -3);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Maze")
        {
            Debug.Log("Exit Maze");
            mainCam.transform.Rotate(new Vector3(-35, 0, 0));
            camControl.offset = new Vector3(0, 20, -14);
        }
    }

    void PlayerLook()
    {
        Ray ray = mainCam.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;

        Debug.DrawRay(ray.origin, ray.direction * 1000, Color.white);                   // Shows Screen To Point Ray in Scene view

        if(Physics.Raycast(ray, out hit, Mathf.Infinity, layerMask))                                    // Raycast
        {
            Vector3 targetPos = new Vector3(hit.point.x, transform.position.y, hit.point.z);

            Quaternion rotation = Quaternion.LookRotation(targetPos - transform.position);

            transform.rotation = Quaternion.Lerp(transform.rotation, rotation, Time.deltaTime * 10f);
        }
    }

    void PlayerMove()
    {
        x = Input.GetAxisRaw("Horizontal");
        z = Input.GetAxisRaw("Vertical");

        animator.SetFloat("Input X", x);
        animator.SetFloat("Input Z", -(z));

        Vector3 dir = new Vector3(x, 0f, z).normalized;
        if (dir.magnitude >= 0.1f)
        {
            controller.Move(dir * speed * Time.deltaTime);
            animator.SetBool("Moving", true);

        }
        else
        {
            animator.SetBool("Moving", false);
        }

        zeroDir.y -= gravity * Time.deltaTime;                        //Applies gravity to character controller
        controller.Move(zeroDir * Time.deltaTime);                    //Applied twice due to gravity being acceleration
    }

    void UpdateHealth()
    {
        healthBar.fillAmount = (health / maxHealth);
    }

    void Die()
    {
        if (health <= 0)
        {
            Debug.Log("Player ded");
            FindPlayers.instance.players.Remove(gameObject);
            //player lose screen & spectate option (IF other player is still alive) could be a ghost option 
        }
    }
}
