﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerShooting : MonoBehaviour
{
    public GameObject arrowPrefab;
    public GameObject sovaArrowPrefab;
    public GameObject miaosplosionPrefab;
    public GameObject boomerangPrefab;

    public Transform bulletSpawn;

    //public enum Projectile { Empty, Sova, Miaosplosion, Boomerang }
    //public Projectile projectile;
    public bool sova = false;
    public bool miaosplosion = false;
    public bool boomerang = false;

    public float shotSpeed = 60f;
    public float autoFireRate = 0.3f;
    float timeStamp = 0f;

    private void Start()
    {
    }

    void FixedUpdate()
    {
        if((Time.time >= timeStamp) && (Input.GetKey(KeyCode.Mouse0)))
        {
            Fire();
            timeStamp = Time.time + autoFireRate;
        }

        if ((Time.time >= timeStamp) && (Input.GetKey(KeyCode.Mouse1)))
        {
            if (sova)
            {
                if (sovaArrowPrefab != null)
                {
                    AltFire(sovaArrowPrefab);
                }
            }
            else if (miaosplosion)
            {
                if (miaosplosionPrefab != null)
                {
                    AltFire(miaosplosionPrefab);
                }
            }
            else if (boomerang)
            {
                if (boomerangPrefab != null)
                {
                    AltFire(boomerangPrefab);
                }
            }
            timeStamp = Time.time + autoFireRate;
        }
    }

    void Fire()             // Fires unlimited normal arrow
    {
        GameObject arrow = Instantiate(arrowPrefab, bulletSpawn.position, bulletSpawn.rotation);

        arrow.GetComponent<Rigidbody>().velocity = arrow.transform.forward * shotSpeed;

        Destroy(arrow, 2.0f);      
    }

    void AltFire(GameObject activePowerup)
    {
        GameObject projectile = Instantiate(activePowerup, bulletSpawn.position, bulletSpawn.rotation);

        projectile.GetComponent<Rigidbody>().velocity = projectile.transform.forward * shotSpeed;

        Destroy(projectile, 5.0f);
    }
}
