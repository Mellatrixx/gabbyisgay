﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCounter : MonoBehaviour
{
    public float goldKeys = 0;
    public float powerUpsCollected = 0;
    public float sovaArrow = 0;
    public float timeAlive = 0;

    private void Update()
    {
        timeAlive += Time.deltaTime;
    }
}
