﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAttack : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Player")
        {
            Debug.Log("Player Hit");
            other.GetComponent<PlayerController>().health -= 1;
            transform.Find("Zombie").GetComponent<Animator>().SetTrigger("attack");
        }
       
    }
}
