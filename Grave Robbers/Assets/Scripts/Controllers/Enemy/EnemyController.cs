﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using System.Linq;

public class EnemyController : MonoBehaviour
{
    [Header("Follow Player")]
    public float viewRadius = 15f;
    public float chaseSpeed = 3.0f;

    [Header("Patrol Radius")]
    public float patrolRadius = 60f;
    public float patrolSpeed = 2.0f;

    [Header("Attack Radius")]
    public float attackRadius = 1.5f;

    Transform target;           // Closest player target
    NavMeshAgent agent;
    Animator anim;
    Collider coll;
    bool spawned = false;

    public float health = 3f;

    Vector3 startPos;

    public bool isPatrolling = false;
    bool isChasing = false;
    bool isAttacking = false;


    GameObject[] playersArray;

    private void Awake()
    {
        coll = GetComponent<Collider>();
        agent = FindObjectOfType<NavMeshAgent>();
        anim = FindObjectOfType<Animator>();
        startPos = transform.position;
    }

    void Start()
    { 
        InvokeRepeating("Patrol", 0f, 6.5f);
    }

    

    // Update is called once per frame
    void Update()
    {
        playersArray = FindPlayers.instance.players.ToArray();      
        Search();
        if (health <= 0)
        {
            Die();
        }

        anim.SetFloat("speed", agent.speed);
    }

    Transform GetClosestPlayer(GameObject[] players)
    {
        Transform bestTarget = null;
        float closestDistanceSqr = Mathf.Infinity;
        Vector3 currentPosition = transform.position;
        foreach (GameObject potentialTarget in players)
        {
            Vector3 directionToTarget = potentialTarget.transform.position - currentPosition;
            float dSqrToTarget = directionToTarget.sqrMagnitude;
            if (dSqrToTarget < closestDistanceSqr)
            {
                closestDistanceSqr = dSqrToTarget;
                bestTarget = potentialTarget.transform;
                Debug.Log("best target acquired");
            }
        }
        return bestTarget;
    }

    void Patrol()
    {
        isAttacking = false;
        isChasing = false;
        isPatrolling = true;
        agent.speed = patrolSpeed;

        Vector3 nextPoint = startPos + new Vector3(Random.Range(-patrolRadius, patrolRadius), 0, Random.Range(-patrolRadius, patrolRadius));
        agent.destination = nextPoint;
        float distance = Vector3.Distance(transform.position, nextPoint);
    }

    void Search()
    {
        target = GetClosestPlayer(playersArray);
        float distance = Vector3.Distance(target.position, transform.position);
        if(distance < viewRadius)
        {
            isPatrolling = false;
            isChasing = true;
            agent.destination = target.position;
            agent.stoppingDistance = 1f;
            
        }
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, viewRadius);
        Gizmos.color = Color.blue;
        Gizmos.DrawWireSphere(transform.position, patrolRadius);
    }

    void Die()
    {
        
        anim.SetBool("death", true);
        Debug.Log("Zom ded");

        ZombieList.instance.enemies.Remove(gameObject);
        coll.enabled = false;
        Destroy(gameObject);
    }
   
}
