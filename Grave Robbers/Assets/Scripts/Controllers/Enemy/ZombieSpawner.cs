﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZombieSpawner : MonoBehaviour
{
    public GameObject zombiePrefab;
    
    bool spawned = false;

    EnemyController enemyController;
    ZombieList instance;

    void Start()
    {
        instance = ZombieList.instance;
        enemyController = GetComponent<EnemyController>();
        InvokeRepeating("Spawn", 6.5f, 6.5f);
    }

    void Spawn()
    {
        if (instance.enemies.Count <= 40 && enemyController.isPatrolling)
        {
            spawned = false;
            Vector3 pos = transform.position;
            Quaternion rot = transform.rotation;

            if (!spawned)
            {
                spawned = true;
                GameObject enemyToSpawn = Instantiate(zombiePrefab, pos + Vector3.one, rot);
            }
        }
    }

}
