﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Photon.Pun;
using Photon.Realtime;

public class PlayerPrefabScript : MonoBehaviour
{
    public Text playerNameText;

    public Player myPlayer { get; private set; }

    public void SetPlayerinfo(Player player)
    {
        myPlayer = player;
        playerNameText.text = player.NickName;
    }
}
