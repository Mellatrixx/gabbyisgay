﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

using Photon.Chat;
using Photon.Pun;
using Photon.Realtime;

public class LobbyManager : MonoBehaviourPunCallbacks
{
    public Text roomName;

    public Transform playerContent;
    [SerializeField]
    private PlayerPrefabScript playerList; //prefab

    private List<PlayerPrefabScript> _Listings = new List<PlayerPrefabScript>();

    public GameObject StartButton;
    //[SerializeField]
   // InputField ChatHere;

    //public GameObject ChatButton;

    // Start is called before the first frame update
    void Start()
    {
        roomName.text = PhotonNetwork.CurrentRoom.Name;
    }
    private void Awake()
    {
        GetCurrentRoomPlayers();
        if (PhotonNetwork.IsMasterClient)
        {
            StartButton.SetActive(true);

        }
        else if (!PhotonNetwork.IsMasterClient)
        {
            StartButton.SetActive(false);
  
        }
    }
    // Update is called once per frame
    void Update()
    {
        PhotonNetwork.AutomaticallySyncScene = true;



    }
    private void GetCurrentRoomPlayers()
    {
        foreach (KeyValuePair<int, Player> playerInfo in PhotonNetwork.CurrentRoom.Players)
        {
            AddPlayerListing(playerInfo.Value);
        }

    }
    private void AddPlayerListing(Player newPlayer)
    {
        PlayerPrefabScript listing = Instantiate(playerList, playerContent);
        if (listing != null)
        {
            listing.SetPlayerinfo(newPlayer);
            _Listings.Add(listing);
        }
    }

    public override void OnPlayerEnteredRoom(Player newPlayer)
    {
        AddPlayerListing(newPlayer);
    }
    public override void OnPlayerLeftRoom(Player otherPlayer)
    {
        base.OnPlayerLeftRoom(otherPlayer);

        int index = _Listings.FindIndex(x => x.myPlayer == otherPlayer);
        if (index != -1)
        {
            Destroy(_Listings[index].gameObject);
            _Listings.RemoveAt(index);
        }
    }
    public void QuitButton()
    {

        PhotonNetwork.LeaveRoom(true);


        PhotonNetwork.LoadLevel(1);
    }
    public void StartGame()
    {
        if (PhotonNetwork.IsMasterClient)
        {


            PhotonNetwork.CurrentRoom.IsOpen = false;
            PhotonNetwork.CurrentRoom.IsVisible = false;
            PhotonNetwork.LoadLevel(3);
        }
      
    }
    public override void OnMasterClientSwitched(Player newMasterClient)
    {
        base.OnMasterClientSwitched(newMasterClient);
        PhotonNetwork.LeaveRoom(true);
        PhotonNetwork.LoadLevel(1);
    }

    public override void OnEnable()
    {
        base.OnEnable();
      
    }
}
