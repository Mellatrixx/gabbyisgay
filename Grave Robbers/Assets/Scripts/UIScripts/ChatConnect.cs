﻿using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChatConnect : MonoBehaviour
{
    public string chatName;

    public void Awake()
    {
        chatName = PhotonNetwork.LocalPlayer.NickName;
        ConnectToChat();
    }
    public void ConnectToChat()
    {
        ChatUI chatui = FindObjectOfType<ChatUI>();
        chatui.userName = this.chatName;    //Trim to get rid of blank space
        if (!string.IsNullOrEmpty(chatui.userName))
        {
            chatui.Connect();
            enabled = false; //disable this script
        }
    }
}
