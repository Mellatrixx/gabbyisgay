﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
    public GameObject Credits;
    public GameObject MainMenuCanvas;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void Exit()
    {
        Application.Quit();
    }
     public void StartGame()
    {
        SceneManager.LoadScene(1);
    }
    public void CreditsButton()
    {
        Credits.SetActive(true);
        MainMenuCanvas.SetActive(false);
    }
    public void BTMM()
    {
        Credits.SetActive(false);
        MainMenuCanvas.SetActive(true);
    }
}
