﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.UI;
using Photon.Chat;
using Photon.Realtime;
using Photon.Pun;

public class ChatUI : MonoBehaviour, IChatClientListener
{
    //Login Panel
    public string userName; //Chat Name you type into the box
    public ChatClient chatClient;   //Photon class passing msg and setup connection
    protected internal AppSettings chatAppSettings;  //link to the PhotonServerSettings 

    //Chat Panel
    public GameObject chatPanel;
    public Text messageText;
    public InputField messageInputField;
    public string[] channelsToAutoJoin; //first join the chat, assign to a channel.
    public int historyLength = 10; //history chat msg, context of the chat

    // Start is called before the first frame update
    void Start()
    {

        this.chatClient = new ChatClient(this);
        userName = PhotonNetwork.NickName;
        DontDestroyOnLoad(this.gameObject); //Create a client talks to the Photon server
        this.chatAppSettings = PhotonNetwork.PhotonServerSettings.AppSettings;
        bool appIDPresent = !string.IsNullOrEmpty(this.chatAppSettings.AppIdChat);    // check the AppID been put into the box

        if (!appIDPresent)
        {
            Debug.LogError("Chat Id is missing in settings.");
        }

        this.chatPanel.SetActive(false);

        Connect();
    }

    // Update is called once per frame
    void Update()
    {
        //To check if the server got new msg for us
        if (this.chatClient != null)    //if client exists
        {
            this.chatClient.Service();  //if new msg coming in
        }
    }

    //Called from button click, connect to the server
    public void Connect()
    {

        this.chatClient.UseBackgroundWorkerForSending = true;
        this.chatClient.Connect(this.chatAppSettings.AppIdChat, "1.0", 
                            new Photon.Chat.AuthenticationValues(this.userName));

        Debug.Log("Connecting as " + this.userName);
    }

    public void DebugReturn(ExitGames.Client.Photon.DebugLevel level, string message) { }


    public void OnDisconnected() { }


    public void OnConnected() 
    {
        Debug.Log("Connected to Server");
        this.chatPanel.SetActive(true); //chat panel pops up after connected
        this.chatClient.Subscribe("Main", 0, historyLength,
           creationOptions: new ChannelCreationOptions { PublishSubscribers = true });   //current users in this room.
    }

    public void SendChatMessage (string message)
    {
        this.chatClient.PublishMessage(this.channelsToAutoJoin[0], message);
        this.messageInputField.text = "";   //clear the msg text box
        //this.messageInputField.ActivateInputField();    //make the cursor to stay in the text box instead of going away
        //this.messageInputField.Select();
    }

    public void OnChatStateChange(ChatState state) { }


    //put msg into the scrollable text box 
    public void OnGetMessages(string channelName, string[] senders, object[] messages) 
    {
        ChatChannel channel = null;
        this.chatClient.TryGetChannel(channelName, out channel);
        this.messageText.text = channel.ToStringMessages();
    }


    public void OnPrivateMessage(string sender, object message, string channelName) { }

    public void PrintSubscribedUsers(string channelName)
    {
        ChatChannel channel = null;
        this.chatClient.TryGetChannel(channelName, out channel);
    }

    //client runs OnSubscribed
    public void OnSubscribed(string[] channels, bool[] results) 
    {
        //when first join
        this.chatClient.PublishMessage(channels[0], "has joined the chat.");
        PrintSubscribedUsers(channels[0]);
    }


    public void OnUnsubscribed(string[] channels) { }


    public void OnStatusUpdate(string user, int status, bool gotMessage, object message) { }


    //Other client runs OnUserSubscribed
    public void OnUserSubscribed(string channel, string user) 
    {
        PrintSubscribedUsers(channel);
    }


    public void OnUserUnsubscribed(string channel, string user) 
    {
        PrintSubscribedUsers(channel);
    }
}
