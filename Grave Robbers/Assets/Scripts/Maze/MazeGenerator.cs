﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MazeGenerator : MonoBehaviour
{
	public int width = 10;
	public int height = 10;

	public GameObject wall;
	public GameObject key;

	private bool keySpawned = false;
	Vector3 offset;			//Generate maze around maze spawner

	private void Awake()
	{
		offset = gameObject.transform.position;
	}
	// Start is called before the first frame update
	void Start()
    {
		GenerateLevel();
    }

	void GenerateLevel()
	{
		// Loop over the grid
		for (int x = 0; x <= width; x += 2)  // For every every 2 units within 0 to width, choose to spawn a 1x1 block
		{
			for (int y = 0; y <= height; y += 2)
			{
				// Should we place a wall?
				if (Random.value > .7f) // Chance to spawn
				{
					// Spawn a wall
					Vector3 pos = new Vector3(x - width / 2f, 1f, y - height / 2f);
					Instantiate(wall, pos + offset, Quaternion.identity, transform);
				}
				else if (Random.value <= 0.5f && x >= width/2  && y >= width/2 && !keySpawned) // Chance to spawn, Centre of maze, key NOT spawned yet
				{
					// Spawn the key
					Vector3 pos = new Vector3(x - width / 2f, 1f, y - height / 2f); // Spawn key
					GameObject Key = Instantiate(key, pos + offset, Quaternion.identity, transform);
					keySpawned = true;
				}
			}
		}
	}
}
