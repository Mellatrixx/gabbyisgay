﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Arrow : MonoBehaviour
{
    PlayerShooting playerShooting;      // Get reference to 

    Collider col;
    Rigidbody rb;

    // Sova Arrow
    [Header("Sova")]
    GameObject viewRad;
    public Vector3 growScale;

    public enum Projectile { Normal, Sova, Miaosplosion, Boomerang };
    public Projectile projectile;


    private void Awake()
    {
        col = GetComponent<Collider>();
        rb = GetComponent<Rigidbody>();

        if (projectile == Projectile.Sova)
        {
            viewRad = transform.Find("ViewVisualisationRadius").gameObject;
        }
    }

    private void Update()
    {
        Sova();
        Miaosplosion();
        Boomerang();
    }

    void OnTriggerEnter(Collider collider) // Effect On Collision
    {
        if (collider.gameObject.tag == "Player")               // Ignore Collision with Player
        {
            return;
        }
        else if (projectile == Projectile.Normal && collider.gameObject.tag == "Enemy")   // Normal Collision effect: Sticks to enemies colliders ONLY
        {                         
            gameObject.transform.parent = collider.transform;   // Make the collider object its parent
            col.enabled = false;                                // Disable your collider, to prevent sticking to anything else
            rb.velocity = Vector3.zero;                         // Stop moving
            rb.useGravity = false;                              // Stop Falling
            collider.transform.Find("Zombie").GetComponent<Animator>().SetTrigger("damage");
            collider.gameObject.GetComponent<EnemyController>().health -= 1;    // Reference to Enemy Controller Health Stat && reduce by 1
        }
        else if (projectile == Projectile.Sova)                 // Sova Collision effect: Sticks to any collider
        {
            gameObject.transform.parent = collider.transform;   // Make the collider object its parent
            col.enabled = false;                                // Disable your collider, to prevent sticking to anything else
            rb.velocity = Vector3.zero;                         // Stop moving
            rb.useGravity = false;                              // Stop Falling
        }
        else if (projectile == Projectile.Miaosplosion)         // Miao Collision effect: Create explosion that damages surrounding enemis
        {
            //explosion vfx
            //generate a collider to damage surrounding zoms?
        }
        else if (projectile == Projectile.Boomerang && collider.gameObject.tag != "Prop") // Boomer Colllision Effect: Destroy self if collides with Static Props
        {
            Destroy(gameObject);
        }
    }

    void Sova()
    {
        if (projectile == Projectile.Sova)
        { 
            viewRad.transform.localScale += growScale * Time.deltaTime;
        }
    }

    void Miaosplosion()
    {

    }

    void Boomerang()
    {
        //
    }
}
