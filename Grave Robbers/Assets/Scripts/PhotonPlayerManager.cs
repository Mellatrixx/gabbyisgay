﻿using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PhotonPlayerManager : MonoBehaviourPunCallbacks
{
    public GameObject PlayerSpawner;

    public GameObject PlayerPrefab;
    // Start is called before the first frame update
    void Start()
    {
        float randomValue = Random.Range(-5, 5);    //spawn at random position
        PhotonNetwork.Instantiate(PlayerPrefab.name, new Vector3(PlayerPrefab.transform.position.x * randomValue, PlayerPrefab.transform.position.y), Quaternion.identity, 0);

    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
