﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZombieList : MonoBehaviour
{
    #region Singleton

    public static ZombieList instance;

    private void Awake()
    {
        instance = this;
    }

    #endregion

    public GameObject zombiePrefab;
   
    public List<GameObject> spawnPoints = new List<GameObject>();

    public List<GameObject> enemies = new List<GameObject>();

    bool spawned = false;

    private void Update()
    {
        AddToList();

        zeroZom();
    }

    public void AddToList()
    {
        foreach (GameObject enemy in GameObject.FindGameObjectsWithTag("Enemy"))
        {
            if (!enemies.Contains(enemy))        //check if enemy is already in list
            {
                enemies.Add(enemy);
            }
        }
    }

    public void zeroZom()
    {
        if (enemies.Count == 0)
        {
            foreach (GameObject spawnLocation in spawnPoints)
            {
                Instantiate(zombiePrefab, spawnLocation.transform.position, spawnLocation.transform.rotation);
            }
           
        }
    }

}
