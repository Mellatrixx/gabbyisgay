﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndLevel : MonoBehaviour
{
    #region Singleton

    public static EndLevel instance;

    private void Awake()
    {
        instance = this;
    }

    #endregion

    public GameObject keyPrefab;
    public List<GameObject> keySpawns;
    public List<GameObject> keyList;
    
    public GameObject doorPrefab;
    public List<GameObject> doorSpawns;
 
    public GameObject gravePrefab;
    public List<GameObject> graveSpawns;

    GameObject end;

    public int keysObtained = 0;
    public int keysNeeded = 3;  

    // Start is called before the first frame update
    void Start()
    {
        RandomSpawn(keyPrefab, keySpawns);
        DoorOrGrave();
        for (int i = 0; i < keySpawns.Count - 1; i++)
        {
            GameObject newKey = Instantiate(keyPrefab, keySpawns[i].transform.position, keySpawns[i].transform.rotation);
            keyList.Add(newKey);
        }
    }

    // Update is called once per frame
    void Update()
    {
        KeyGot();
    }

    void KeyGot()
    {
        if (keysObtained >= keysNeeded)
        {
            end.SetActive(true);
            
        }

        
    }

    GameObject RandomSpawn(GameObject objectToSpawn, List<GameObject> spawnList)
    {
        GameObject placeToSpawn = spawnList[Random.Range(0, spawnList.Count - 1)];
        GameObject objSpawned = Instantiate(objectToSpawn, placeToSpawn.transform.position, transform.rotation);
        return objSpawned;
    }

    void DoorOrGrave()
    {
        int doorOrGrave = Mathf.RoundToInt(Random.value);
        if (doorOrGrave == 0)
        {
            end = RandomSpawn(doorPrefab, doorSpawns);
            end.SetActive(false);
        }
        else
        {
            end = RandomSpawn(gravePrefab, graveSpawns);
            end.SetActive(false);
        }
    }

  
}
