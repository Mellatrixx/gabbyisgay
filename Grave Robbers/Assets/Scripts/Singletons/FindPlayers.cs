﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FindPlayers : MonoBehaviour
{
    #region Singleton

    public static FindPlayers instance;

    private void Awake()
    {
        instance = this;
    }

    #endregion

    public List<GameObject> players = new List<GameObject>();

    void Update()
    {
        foreach(GameObject player in GameObject.FindGameObjectsWithTag("Player"))       //creates list of all players
        {
            if (!players.Contains(player))
            { 
                players.Add(player);
            }
        }
    }
}
